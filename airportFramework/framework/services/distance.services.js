import supertest from 'supertest';
import { accessData } from '../config';
import { urls } from '../config/urls';

const DistanceRequest = function Request() {
    this.getDistance = async function distance(from, to) {
        return supertest(urls.base).post(`/airports/distance`).set('Authorization', 'Bearer token=' + accessData.apiKey)
            .set('Accept', 'application/json').send('from=' + from + '&to=' + to);
    };
};

export { DistanceRequest };
