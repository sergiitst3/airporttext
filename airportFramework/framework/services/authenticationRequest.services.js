import supertest from 'supertest';
import { accessData } from '../config';
import { urls } from '../config/urls';

const AuthenticationRequest = function Request() {
    this.getToken = async function token() {
        return supertest(urls.base).post(`/tokens`).set('Authorization', 'Bearer token=' + accessData.apiKey)
            .set('Accept', 'application/json').send('email=' + accessData.email + '&password=' + accessData.password);
    };
    this.getTokenWithCredentials = async function token(email, password) {
        return supertest(urls.base).post(`/tokens`).set('Authorization', 'Bearer token=' + accessData.apiKey)
            .set('Accept', 'application/json').send('email=' + email + '&password=' + password);
    };
    this.getEndpointGet = async function endpointsGet(endpoint){
        return supertest(urls.base).get(endpoint).set('Authorization', 'Bearer token=')
                .set('Accept', 'application/json');
    };
};

export { AuthenticationRequest };
