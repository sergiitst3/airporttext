import supertest from 'supertest';
import { accessData } from '../config';
import { urls } from '../config/urls';

const FavoritesRequest = function Request() {
    this.addToFavorites = async function addFavorite(iata, note) {
        return supertest(urls.base).post(`/favorites`).set('Authorization', 'Bearer token=' + accessData.apiKey)
            .set('Accept', 'application/json').send('airport_id=' + iata + '&note=' + note);
    };
    this.getFavoritesList = async function favoritesList() {
        return supertest(urls.base).get(`/favorites`).set('Authorization', 'Bearer token=' + accessData.apiKey)
            .set('Accept', 'application/json');
    };
    this.deleteFromFavorites = async function deleteFromFavorites(favoriteId) {
        return supertest(urls.base).delete(`/favorites/` + favoriteId).set('Authorization',
            'Bearer token=' + accessData.apiKey);
    };
    this.clearFavorites = async function clear(){
        const result = await this.getFavoritesList();
        for (let i = 0; i < result.body.data.length; i++) {
            await this.deleteFromFavorites(result.body.data[i].id);
        }
    };
};

export { FavoritesRequest };
