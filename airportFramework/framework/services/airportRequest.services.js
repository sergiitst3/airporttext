import supertest from 'supertest';
import { accessData } from '../config';
import { urls } from '../config/urls';

const AirportRequest = function Request() {
  this.getList = async function list() {
    return supertest(urls.base).get(`/airports`).set('Authorization', 'Bearer token=' + accessData.apiKey)
      .set('Accept', 'application/json');
  };
  this.getById = async function recordByIata(iata) {
    return supertest(urls.base).get(`/airports/${iata}`).set('Authorization', 'Bearer token=' + accessData.apiKey)
        .set('Accept', 'application/json');
  };
};

export { AirportRequest };
