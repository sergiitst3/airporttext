import { AuthenticationRequest, FavoritesRequest, AirportRequest, DistanceRequest } from './services/index'

const apiProvider = () => ({
    auth: () => new AuthenticationRequest(),
    favorites: () => new FavoritesRequest(),
    airports: () => new AirportRequest(),
    distances: () => new DistanceRequest()
});

export { apiProvider };
