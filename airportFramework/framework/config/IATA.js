const iata = {
    Kiev: 'KBP',
    Osaka: 'KIX',
    Goroka: 'GKA',
    Mount_Hagen: 'HGU'
};

export { iata };
