import { test } from '@jest/globals';
import { apiProvider } from '../framework/index';
import { iata } from "../framework/config/IATA";

describe('Airport distances tests', () => {
    test('Get distance between two airports', async () => {
        const result = await apiProvider().distances().getDistance(iata.Kiev, iata.Osaka);
        expect(result.body.data.id).toBe(iata.Kiev + '-' + iata.Osaka);
        expect(result.body.data.type).toBe('airport_distance');
        const resultAttributes = result.body.data.attributes;
        expect(resultAttributes.hasOwnProperty('from_airport')).toBeTruthy();
        expect(resultAttributes.hasOwnProperty('to_airport')).toBeTruthy();
        expect(resultAttributes.hasOwnProperty('kilometers')).toBeTruthy();
        expect(resultAttributes.hasOwnProperty('miles')).toBeTruthy();
        expect(resultAttributes.hasOwnProperty('nautical_miles')).toBeTruthy();
    });
    it.each`
    from          |  to             
    ${iata.Kiev}  | ${''}   
    ${'ABCD'}     | ${iata.Osaka}
    ${''}         | ${'213'} 
    ${''}         | ${''} 
  `('Invalid distance requests', async ({from, to}) => {
        const result = await apiProvider().distances().getDistance(from, to);
        expect(result.status).toBe(422);
        expect(result.body.errors.length).toBe(1);
        const errorObject = result.body.errors[0];
        expect(errorObject.title).toBe('Unable to process request');
        expect(errorObject.detail).toBe("Please enter valid 'from' and 'to' airports.");
    });
})
