import { test } from '@jest/globals';
import { apiProvider } from '../framework/index';

describe('Base airports service tests', () => {
  test('Get list of airports', async () => {
    const result = await apiProvider().airports().getList();
    expect(result.status).toBe(200);
    expect(result.body.data.length).toBe(30);
  });
  it('Check airport record', async () => {
    const airportsList = await apiProvider().airports().getList();
    const airportRecord = airportsList.body.data[0];
    expect(airportRecord.id.length).toBe(3);
    expect(airportRecord.type).toBe('airport');
    expect(airportRecord.attributes.hasOwnProperty('name')).toBeTruthy();
    expect(airportRecord.attributes.hasOwnProperty('city')).toBeTruthy();
    expect(airportRecord.attributes.hasOwnProperty('country')).toBeTruthy();
    expect(airportRecord.attributes.hasOwnProperty('iata')).toBeTruthy();
    expect(airportRecord.attributes.hasOwnProperty('icao')).toBeTruthy();
    expect(airportRecord.attributes.hasOwnProperty('latitude')).toBeTruthy();
    expect(airportRecord.attributes.hasOwnProperty('longitude')).toBeTruthy();
    expect(airportRecord.attributes.hasOwnProperty('altitude')).toBeTruthy();
    expect(airportRecord.attributes.hasOwnProperty('timezone')).toBeTruthy();
  });
  it('Get airport by id', async () => {
    const airportRecordsList = await apiProvider().airports().getList();
    const airportRecord = airportRecordsList.body.data[0];
    const airportIata = airportRecord.attributes.iata;
    const result = await apiProvider().airports().getById(airportIata);
    expect(result.body.data.id).toBe(airportRecord.id);
  });
  it.each`
    request
    ${'123'}
    ${'ABCD'}
    ${'_A_'}
  `('Testing invalid requests', async ({ request }) => {
    const result = await apiProvider().airports().getById(request);
    expect(result.body.errors.length).toBe(1);
    const errorBlock = result.body.errors[0];
    expect(result.status).toBe(404);
    expect(errorBlock.title).toBe('Not Found');
    expect(errorBlock.detail).toBe('The page you requested could not be found');
  });
})
