import { test } from '@jest/globals';
import { apiProvider } from '../framework/index';
import { accessData } from "../framework/config";

describe('Authentication verification tests', () => {
    test('Get user API token', async () => {
        const result = await apiProvider().auth().getToken();
        expect(result.body.token).toBe(accessData.apiKey);
    });
    it.each`
    email                |  password             
    ${accessData.email}  | ${''}   
    ${''}                | ${accessData.password}
    ${'123'}             | ${'123'} 
    ${''}                | ${''} 
  `('Attempt to get token without proper data', async ({ email, password }) => {
        const result = await apiProvider().auth().getTokenWithCredentials(email, password);
        expect(result.status).toBe(401);
        expect(result.body.errors.length).toBe(1);
        expect(result.body.errors[0].title).toBe('Unauthorized');
        expect(result.body.errors[0].detail).toBe('You are not authorized to perform the requested action.');
    });
    it.each`
    endpoint
    ${'/favorites'}
  `('Attempt to access endpoints without token', async ({ endpoint }) => {
        const result = await apiProvider().auth().getEndpointGet(endpoint);
        expect(result.status).toBe(401);
        expect(result.body.errors.length).toBe(1);
        expect(result.body.errors[0].title).toBe('Unauthorized');
        expect(result.body.errors[0].detail).toBe('You are not authorized to perform the requested action.');
    });
})
