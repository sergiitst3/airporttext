import {afterEach, test} from '@jest/globals';
import { apiProvider } from '../framework/index';
import {iata} from "../framework/config/IATA";

describe('Favorites services tests', () => {
    afterEach(async () => {
        await apiProvider().favorites().clearFavorites();
    })
    test('Save airport to favorites', async () => {
        const result = await apiProvider().favorites().addToFavorites(iata.Kiev);
        const record = result.body;
        expect(record.data.type).toBe('favorite');
        expect(record.data.attributes.note).toBe('undefined');
    });
    it('Add favorite with note', async () => {
        const result = await apiProvider().favorites().addToFavorites(iata.Kiev, 'someNote');
        const record = result.body;
        expect(record.data.type).toBe('favorite');
        expect(record.data.attributes.note).toBe('someNote');
    })
})
